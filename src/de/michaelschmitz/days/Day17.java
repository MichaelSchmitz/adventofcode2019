package de.michaelschmitz.days;

import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.function.Predicate.isEqual;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toCollection;

public class Day17 implements Day {
    private static final boolean PRINT_MAP = false;

    private Collection<String> getMoves(Map<Position, Tile> map, Map<Tile, Double> angles) {
        List<String> path = new LinkedList<>();
        Position previous = map.entrySet().stream().filter(e -> angles.containsKey(e.getValue())).findFirst().map(Map.Entry::getKey).orElse(null); //start from robot position
        if (previous == null) return path;
        double angle = angles.get(map.get(previous));
        map.values().removeIf(isEqual(Tile.SCAFFOLD).negate());
        Set<Position> positions = new LinkedHashSet<>();
        while (map.size() != positions.size()) {
            Position next = previous.getNearbyPoints().filter(pos -> !positions.contains(pos)).filter(map::containsKey).findFirst().orElse(null);
            if (next == null) return path;
            double nextAngle = Math.atan2(previous.y - next.y, next.x - previous.x);
            path.add(Math.sin(nextAngle - angle) == 1D ? "L" : "R");
            angle = nextAngle;
            int moves = 0;
            while (map.containsKey(next)) {
                positions.add(next);
                moves++;
                previous = next;
                next = next.nextInLine(angle);
            }
            path.add(String.valueOf(moves));
        }
        return path;
    }

    private Collection<String> splitToSubcommands(String pathCommand) {
        List<String> subcommands = new LinkedList<>();
        while (!pathCommand.isEmpty()) {
            String command = pathCommand;
            var test = IntStream.rangeClosed(10, 20)
                    .mapToObj(i -> command.substring(0, Math.min(i, command.length()))).toList();
            var test2 =IntStream.rangeClosed(10, 20)
                    .mapToObj(i -> command.substring(0, Math.min(i, command.length())))
                    .max(Comparator.comparing(countOccurances(pathCommand)).thenComparing(String::length));
            String subcommand = IntStream.rangeClosed(10, 20)
                    .mapToObj(i -> command.substring(0, Math.min(i, command.length())))
                    .max(Comparator.comparing(countOccurances(pathCommand)).thenComparing(String::length))
                    .orElse(null);
            if (subcommand == null) return subcommands;
            subcommand = subcommand.startsWith(",") ? subcommand.replaceFirst(",", "") : subcommand;
            subcommand = subcommand.endsWith(",") ? subcommand.substring(0, subcommand.length() - 1) : subcommand;
            subcommands.add(subcommand);
            pathCommand = pathCommand.replaceAll(subcommand, "");
            pathCommand = pathCommand.replaceAll(",,", ",");
            pathCommand = pathCommand.startsWith(",") ? pathCommand.replaceFirst(",", "") : pathCommand;
            pathCommand = pathCommand.endsWith(",") ? pathCommand.substring(0, pathCommand.length() - 1) : pathCommand;
        }
        return subcommands;
    }

    private Function<String, Integer> countOccurances(String command) {
        return subcommand -> command.split(subcommand, -1).length - 1;
    }


    private boolean isIntersection(Map<Position, Tile> map, Map.Entry<Position, Tile> entry) {
        return entry.getValue() == Tile.SCAFFOLD && entry.getKey().getNearbyPoints().map(position -> map.getOrDefault(position, Tile.SPACE)).allMatch(isEqual(Tile.SCAFFOLD));
    }

    private void printMap(Map<Position, Tile> map) {
        if (PRINT_MAP) {
            int maxX = map.keySet().stream().max(Comparator.comparingInt(pos -> pos.x)).get().x;
            int maxY = map.keySet().stream().max(Comparator.comparingInt(pos -> pos.y)).get().y;
            int minX = map.keySet().stream().min(Comparator.comparingInt(pos -> pos.x)).get().x;
            int minY = map.keySet().stream().min(Comparator.comparingInt(pos -> pos.y)).get().y;
            for (int y = minY; y <= maxY; y++) {
                for (int x = minX; x <= maxX; x++) {
                    System.out.print(map.getOrDefault(new Position(x, y), Tile.INVALID));
                }
                System.out.println();
            }
        }
    }

    private long runCode(Map<Position, Tile> map, int start, List<Long> commands, String input, boolean driveRobot) {
        try {
            LinkedBlockingQueue<Long> queue1 = new LinkedBlockingQueue<>();
            LinkedBlockingQueue<Long> queue2 = new LinkedBlockingQueue<>();
            IntcodeThread intcodeThread = new IntcodeThread(queue1, queue2, start, input);
            ControlThread controlThread = new ControlThread(queue2, queue1, commands, map);
            controlThread.start();
            intcodeThread.start();
            intcodeThread.join();
            if (!driveRobot)
                controlThread.runControl = false;
            controlThread.join();
            return controlThread.result;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public long executePartOne(List<String> input) {
        Map<Position, Tile> map = new HashMap<>();
        runCode(map, 1, null, input.get(0), false);
        printMap(map);
        return map.entrySet().stream().filter(entry -> isIntersection(map, entry)).map(Map.Entry::getKey).mapToInt(Position::getXY).sum();
    }

    @Override
    public long executePartTwo(List<String> input) {
        Map<Position, Tile> map = new HashMap<>();
        Map<Tile, Double> angles = new HashMap<>();
        runCode(map, 1, null, input.get(0), false);
        angles.put(Tile.ROBOT_RIGHT, 0D);
        angles.put(Tile.ROBOT_TOP, Math.PI / 2);
        angles.put(Tile.ROBOT_DOWN, -Math.PI / 2);
        angles.put(Tile.ROBOT_LEFT, Math.PI);
        Collection<String> moves = getMoves(map, angles);
        String mainRoutine = String.join(",", moves);
        Collection<String> subcommands = splitToSubcommands(mainRoutine);
        LinkedList<String> functionsNames = Stream.of("ABC".split("")).collect(toCollection(LinkedList::new));
        for (String subcommand : subcommands)
            mainRoutine = mainRoutine.replace(subcommand, functionsNames.removeFirst());

        LinkedList<Long> commands = Stream.of(mainRoutine, String.join("\n", subcommands), "n").collect(joining("\n", "", "\n")).chars()
                .mapToObj(Long::valueOf).collect(toCollection(LinkedList::new));

        return runCode(map, 2, commands, input.get(0), true);
    }

    public enum Tile {
        SCAFFOLD,
        SPACE,
        ROBOT_LEFT,
        ROBOT_TOP,
        ROBOT_DOWN,
        ROBOT_RIGHT,
        NEW_LINE,
        INVALID;

        public static Tile getTile(int tile) {
            return switch (tile) {
                case 35 -> SCAFFOLD;
                case 46 -> SPACE;
                case 60 -> ROBOT_LEFT;
                case 94 -> ROBOT_TOP;
                case 62 -> ROBOT_RIGHT;
                case 118 -> ROBOT_DOWN;
                case 10 -> NEW_LINE;
                default -> INVALID;
            };
        }


        @Override
        public String toString() {
            return switch (this) {
                case SPACE -> ".";
                case SCAFFOLD -> "#";
                case INVALID -> "?";
                case NEW_LINE -> "\n";
                case ROBOT_TOP -> "^";
                case ROBOT_DOWN -> "v";
                case ROBOT_LEFT -> "<";
                case ROBOT_RIGHT -> ">";
            };
        }
    }

    private static class Position {
        int x;
        int y;

        Position(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public Stream<Position> getNearbyPoints() {
            return IntStream.rangeClosed(0, 360).mapToDouble(Math::toRadians).mapToObj(this::nextInLine);
        }


        public Position nextInLine(double angle) {
            return new Position(x + (int) Math.cos(angle), y - (int) Math.sin(angle));
        }


        public int getXY() {
            return x * y;
        }

        @Override
        public boolean equals(Object o) {
            if (o instanceof Position) {
                return this.x == ((Position) o).x && this.y == ((Position) o).y;
            }
            return false;
        }

        @Override
        public int hashCode() {
            return (x + "," + y).hashCode();
        }
    }

    private static class ControlThread extends Thread {
        BlockingQueue<Long> in;
        BlockingQueue<Long> out;
        List<Long> commands;
        final Map<Position, Tile> map;
        boolean runControl = true;
        long result = -1;

        ControlThread(BlockingQueue<Long> in, BlockingQueue<Long> out, List<Long> commands, Map<Position, Tile> map) {
            this.in = in;
            this.out = out;
            this.commands = commands;
            this.map = map;
        }

        @Override
        public void run() {
            try {
                int x = 0;
                int y = 0;
                if (commands != null) {
                    for (long l : commands) {
                        out.put(l);
                    }
                }
                while (runControl) {
                    Long input = in.poll(2, TimeUnit.MILLISECONDS);
                    if (input == null)
                        continue;
                    if (input > 0xFF) {
                        result = input;
                        runControl = false;
                    } else {
                        Tile tile = Tile.getTile(input.intValue());
                        if (tile == Tile.NEW_LINE) {
                            y++;
                            x = 0;
                        } else {
                            synchronized (map) {
                                map.put(new Position(x, y), tile);
                            }
                            x++;
                        }
                    }
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }

    private static class IntcodeThread extends Thread {
        long relativeBase = 0;
        BlockingQueue<Long> in;
        BlockingQueue<Long> out;
        long start;
        String input;
        long result;

        IntcodeThread(BlockingQueue<Long> in, BlockingQueue<Long> out, int start, String input) {
            this.in = in;
            this.out = out;
            this.start = start;
            this.input = input;
        }

        public void run() {
            try {
                result = doProcessing();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        private IntcodeThread.Instruction getInstruction(long inst, Map<Long, Long> op, long ip, int size) {
            long val1 = getParam(op, ip, getMode(inst, 1), 1, inst % 10 == 3);
            long val2 = -1;
            long val3 = -1;
            if (size >= 2)
                val2 = getParam(op, ip, getMode(inst, 2), 2, inst % 10 == 3);
            if (size >= 3)
                val3 = getParam(op, ip, getMode(inst, 3), 3, true);
            return new IntcodeThread.Instruction(val1, val2, val3);
        }

        private long getParam(Map<Long, Long> op, long ip, int mode, int pos, boolean rel) {
            Long tmp = op.getOrDefault(ip + pos, 0L);
            switch (mode) {
                case 0:
                    return rel ? tmp : op.getOrDefault(tmp, 0L);
                case 1:
                    return tmp;
                case 2:
                    return rel ? relativeBase + tmp : op.getOrDefault(relativeBase + tmp, 0L);
                default:
                    System.err.println("Wrong mode");
                    return -1;
            }
        }

        private int getMode(long inst, int pos) {
            return (int) (inst / Math.pow(10, pos + 1) % 10);
        }

        long doProcessing() throws InterruptedException {
            String[] opTmp = input.split(",");
            Map<Long, Long> op = new HashMap<>();
            for (long i = 0; i < opTmp.length; i++) {
                op.put(i, Long.parseLong(opTmp[(int) i]));
            }
            op.put(0L, start);
            for (long i = 0; i < op.size(); ) {
                long currentCode = op.getOrDefault(i, 0L);
                switch ((int) (currentCode % 100)) {
                    case 1 -> {
                        Instruction inst = getInstruction(currentCode, op, i, 3);
                        op.put(inst.target, inst.val1 + inst.val2);
                        i += 4;
                    }
                    case 2 -> {
                        Instruction inst = getInstruction(currentCode, op, i, 3);
                        op.put(inst.target, inst.val1 * inst.val2);
                        i += 4;
                    }
                    case 3 -> {
                        Instruction inst = getInstruction(currentCode, op, i, 1);
                        op.put(inst.val1, in.take());
                        i += 2;
                    }
                    case 4 -> {
                        Instruction inst = getInstruction(currentCode, op, i, 1);
                        out.put(inst.val1);
                        i += 2;
                    }
                    case 5 -> {
                        Instruction inst = getInstruction(currentCode, op, i, 2);
                        if (inst.val1 != 0L) {
                            i = inst.val2;
                        } else {
                            i += 3;
                        }
                    }
                    case 6 -> {
                        Instruction inst = getInstruction(currentCode, op, i, 2);
                        if (inst.val1 == 0L) {
                            i = inst.val2;
                        } else {
                            i += 3;
                        }
                    }
                    case 7 -> {
                        Instruction inst = getInstruction(currentCode, op, i, 3);
                        op.put(inst.target, inst.val1 < inst.val2 ? 1L : 0L);
                        i += 4;
                    }
                    case 8 -> {
                        Instruction inst = getInstruction(currentCode, op, i, 3);
                        op.put(inst.target, inst.val1 == inst.val2 ? 1L : 0L);
                        i += 4;
                    }
                    case 9 -> {
                        relativeBase += getInstruction(currentCode, op, i, 1).val1;
                        i += 2;
                    }
                    case 99 -> {
                        return op.getOrDefault(0L, 0L);
                    }
                    default -> {
                        System.err.println("False opcode");
                        return -1L;
                    }
                }
            }
            return -1L;
        }

        private static class Instruction {
            long val1;
            long val2;
            long target;

            Instruction(long val1, long val2, long target) {
                this.val1 = val1;
                this.val2 = val2;
                this.target = target;
            }
        }
    }
}
