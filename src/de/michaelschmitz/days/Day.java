package de.michaelschmitz.days;

import java.util.List;

public interface Day {
    long executePartOne(List<String> input);

    long executePartTwo(List<String> input);
}
