package de.michaelschmitz.days;

import java.util.Arrays;
import java.util.List;

public class Day02 implements Day {
    static int process(int a, int b, int[] op) {
        op[1] = a;
        op[2] = b;
        for (int i = 0; i < op.length; i += 4) {
            switch (op[i]) {
                case 1 -> op[op[i + 3]] = op[op[i + 1]] + op[op[i + 2]];
                case 2 -> op[op[i + 3]] = op[op[i + 1]] * op[op[i + 2]];
                case 99 -> {
                    return op[0];
                }
                default -> {
                    return -1;
                }
            }
        }
        return -1;
    }

    @Override
    public long executePartOne(List<String> input) {
        int[] op = Arrays.stream(input.get(0).split(",")).mapToInt(Integer::parseInt).toArray();
        return process(12, 2, op);
    }

    @Override
    public long executePartTwo(List<String> input) {
        int[] op = Arrays.stream(input.get(0).split(",")).mapToInt(Integer::parseInt).toArray();
        for (int x = 0; x < 100; x++) {
            for (int y = 0; y < 100; y++) {
                if (19690720 == process(x, y, op.clone())) {
                    return x * 100 + y;
                }
            }
        }
        return -1;
    }
}
