package de.michaelschmitz.days;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Stream;

public class Day15 implements Day {
    public final static boolean PRINT_MAP = false;
    private static void printMap(Map<Position, Node> map) {
        if (PRINT_MAP) {
            try {
                new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
            } catch (IOException | InterruptedException ignored) {
            }
            int maxX = map.keySet().stream().max(Comparator.comparingInt(pos -> pos.x)).get().x;
            int maxY = map.keySet().stream().max(Comparator.comparingInt(pos -> pos.y)).get().y;
            int minX = map.keySet().stream().min(Comparator.comparingInt(pos -> pos.x)).get().x;
            int minY = map.keySet().stream().min(Comparator.comparingInt(pos -> pos.y)).get().y;
            for (int y = maxY; y >= minY; y--) {
                for (int x = minX; x <= maxX; x++) {
                    System.out.print(map.getOrDefault(new Position(x, y), new Node()).getChar());
                }
                System.out.println();
            }
        }
    }

    private long runCode(Map<Position, Node> map, String input, boolean oxygenTime) {
        try {
            LinkedBlockingQueue<Long> queue1 = new LinkedBlockingQueue<>();
            LinkedBlockingQueue<Long> queue2 = new LinkedBlockingQueue<>();
            IntcodeThread intcodeThread = new IntcodeThread(queue1, queue2, input);
            ControlThread controlThread = new ControlThread(queue2, queue1, map, oxygenTime);
            controlThread.start();
            intcodeThread.start();
            intcodeThread.join();
            controlThread.join();
            return controlThread.result;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public long executePartOne(List<String> input) {
        Map<Position, Node> map = new HashMap<>();
        return runCode(map, input.get(0), false);
    }

    @Override
    public long executePartTwo(List<String> input) {
        Map<Position, Node> map = new HashMap<>();
        return runCode(map, input.get(0), true);
    }

    public enum Tile {
        START(3),
        WALL(0),
        VISITED(1),
        OXYGEN(2),
        INVALID(-1);

        private final int tile;

        Tile(int tile) {
            this.tile = tile;
        }

        public static Tile getTile(int tile) {
            return switch (tile) {
                case 0 -> WALL;
                case 1 -> VISITED;
                case 2 -> OXYGEN;
                case 3 -> START;
                default -> INVALID;
            };
        }

        public char getChar() {
            return switch (tile) {
                case 3 -> 'x';
                case 1 -> ' ';
                case 2 -> '●';
                case 0 -> '█';
                default -> '0';
            };
        }
    }

    private static class Node {
        Node north;
        Node east;
        Node south;
        Node west;
        Tile tile;

        public Node() {
            tile = Tile.getTile(-1);
        }

        public Node(Tile tile) {
            this.tile = tile;
        }

        public char getChar() {
            return tile.getChar();
        }
    }

    private static class Position {
        int x;
        int y;

        Position(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public boolean equals(Object o) {
            if (o instanceof Position) {
                return this.x == ((Position) o).x && this.y == ((Position) o).y;
            }
            return false;
        }

        @Override
        public int hashCode() {
            return (x + "," + y).hashCode();
        }
    }

    private static class ControlThread extends Thread {
        BlockingQueue<Long> in;
        BlockingQueue<Long> out;
        long result;
        boolean runControl = true;
        boolean oxygenTime;

        final Map<Position, Node> map;

        ControlThread(BlockingQueue<Long> in, BlockingQueue<Long> out, Map<Position, Node> map, boolean oxygenTime) {
            this.in = in;
            this.out = out;
            this.map = map;
            this.oxygenTime = oxygenTime;
        }

        private Node updateGraph(int x, int y, Tile tile) {
            synchronized (map) {
                Position p = new Position(x, y);
                map.putIfAbsent(p, new Node());
                Node node = map.get(p);
                node.tile = tile;
                Position left = new Position(x - 1, y);
                Position right = new Position(x + 1, y);
                Position top = new Position(x, y + 1);
                Position down = new Position(x, y - 1);
                if (map.containsKey(left)) {
                    node.west = map.get(left);
                    map.get(left).east = node;
                }
                if (map.containsKey(right)) {
                    node.east = map.get(right);
                    map.get(right).west = node;
                }
                if (map.containsKey(down)) {
                    node.south = map.get(down);
                    map.get(down).north = node;
                }
                if (map.containsKey(top)) {
                    node.north = map.get(top);
                    map.get(top).south = node;
                }
                return node;
            }
        }

        private void calculateOx() {
            synchronized (map) {
                Node start = map.get(new Position(0, 0));
                result = getMin(start, "");
            }
        }

        private void calculateOxTime() {
            synchronized (map) {
                List<Node> newOx = new LinkedList<>();
                newOx.add(map.entrySet().stream().filter(entry -> entry.getValue().tile == Tile.OXYGEN).findFirst().get().getValue());
                int count = -1;
                while (map.values().stream().anyMatch(node -> node.tile == Tile.VISITED || node.tile == Tile.START)) {
                    newOx = getAdjacent(newOx);
                    count++;
                    printMap(map);
                }
                result = count;
            }
        }

        private List<Node> getAdjacent(List<Node> cur) {
            List<Node> newOx = new LinkedList<>();
            for (Node n : cur) {
                n.tile = Tile.OXYGEN;
                if (n.west != null && (n.west.tile == Tile.VISITED || n.west.tile == Tile.START))
                    newOx.add(n.west);
                if (n.east != null && (n.east.tile == Tile.VISITED || n.east.tile == Tile.START))
                    newOx.add(n.east);
                if (n.north != null && (n.north.tile == Tile.VISITED || n.north.tile == Tile.START))
                    newOx.add(n.north);
                if (n.south != null && (n.south.tile == Tile.VISITED || n.south.tile == Tile.START))
                    newOx.add(n.south);
            }
            return newOx;
        }

        private int getMin(Node n, String dir) {
            if (n == null || n.tile == Tile.WALL)
                return -1;
            if (n.tile == Tile.OXYGEN)
                return 0;
            int west = -1;
            int east = -1;
            int north = -1;
            int south = -1;
            if (!"east".equals(dir))
                west = getMin(n.west, "west") + 1;
            if (!"west".equals(dir))
                east = getMin(n.east, "east") + 1;
            if (!"south".equals(dir))
                north = getMin(n.north, "north") + 1;
            if (!"north".equals(dir))
                south = getMin(n.south, "south") + 1;
            return Stream.of(west, east, north, south).filter(i -> i > 0).min(Comparator.comparingInt(i -> i)).orElse(-1);
        }

        @Override
        public void run() {
            try {
                int x = 0;
                int y = 0;
                synchronized (map) {
                    map.put(new Position(0, 0), new Node(Tile.START));
                }
                Stack<MoveCommand> lastInverted = new Stack<>();
                lastInverted.push(MoveCommand.SOUTH);
                out.put(MoveCommand.NORTH.getValue());
                MoveCommand lastOp = MoveCommand.NORTH;
                while (runControl) {
                    Tile tile = Tile.getTile(in.take().intValue());
                    if (tile == null) {
                        System.err.println("Invalid tile");
                        return;
                    }
                    int tmpX = x;
                    int tmpY = y;
                    switch (lastOp) {
                        case NORTH -> tmpY++;
                        case EAST -> tmpX++;
                        case SOUTH -> tmpY--;
                        case WEST -> tmpX--;
                    }
                    Node node = updateGraph(tmpX, tmpY, tile);
                    if (tile != Tile.WALL) {
                        x = tmpX;
                        y = tmpY;
                    } else {
                        synchronized (map) {
                            node = map.get(new Position(x, y));
                        }
                        lastInverted.pop();
                    }
                    MoveCommand movement = getMovement(node, lastInverted);
                    if (movement == MoveCommand.LAST) {
                        if (lastInverted.empty()) {
                            if (oxygenTime) {
                                calculateOxTime();
                            } else {
                                calculateOx();
                            }
                            runControl = false;
                        } else {
                            movement = lastInverted.pop();
                        }
                    }
                    out.put(movement.getValue());
                    lastOp = movement;
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }

        MoveCommand getMovement(Node node, Stack<MoveCommand> lastInverted) {
            if (node.tile != Tile.WALL) {
                if (node.north == null) {
                    lastInverted.push(MoveCommand.SOUTH);
                    return MoveCommand.NORTH;
                } else if (node.east == null) {
                    lastInverted.push(MoveCommand.WEST);
                    return MoveCommand.EAST;
                } else if (node.south == null) {
                    lastInverted.push(MoveCommand.NORTH);
                    return MoveCommand.SOUTH;
                } else if (node.west == null) {
                    lastInverted.push(MoveCommand.EAST);
                    return MoveCommand.WEST;
                }
            }
            return MoveCommand.LAST;
        }

        enum MoveCommand {
            NORTH(1),
            SOUTH(2),
            WEST(3),
            EAST(4),
            LAST(-1);

            private final int value;

            MoveCommand(final int value) {
                this.value = value;
            }

            long getValue() {
                return value;
            }
        }
    }


    private static class IntcodeThread extends Thread {
        long relativeBase = 0;
        BlockingQueue<Long> in;
        BlockingQueue<Long> out;
        String input;
        long result;

        IntcodeThread(BlockingQueue<Long> in, BlockingQueue<Long> out, String input) {
            this.in = in;
            this.out = out;
            this.input = input;
        }

        public void run() {
            try {
                result = doProcessing();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        private Instruction getInstruction(long inst, Map<Long, Long> op, long ip, int size) {
            long val1 = getParam(op, ip, getMode(inst, 1), 1, inst % 10 == 3);
            long val2 = -1;
            long val3 = -1;
            if (size >= 2)
                val2 = getParam(op, ip, getMode(inst, 2), 2, inst % 10 == 3);
            if (size >= 3)
                val3 = getParam(op, ip, getMode(inst, 3), 3, true);
            return new Instruction(val1, val2, val3);
        }

        private long getParam(Map<Long, Long> op, long ip, int mode, int pos, boolean rel) {
            Long tmp = op.getOrDefault(ip + pos, 0L);
            switch (mode) {
                case 0:
                    return rel ? tmp : op.getOrDefault(tmp, 0L);
                case 1:
                    return tmp;
                case 2:
                    return rel ? relativeBase + tmp : op.getOrDefault(relativeBase + tmp, 0L);
                default:
                    System.err.println("Wrong mode");
                    return -1;
            }
        }

        private int getMode(long inst, int pos) {
            return (int) (inst / Math.pow(10, pos + 1) % 10);
        }

        long doProcessing() throws InterruptedException {
            String[] opTmp = input.split(",");
            Map<Long, Long> op = new HashMap<>();
            for (long i = 0; i < opTmp.length; i++) {
                op.put(i, Long.parseLong(opTmp[(int) i]));
            }
            for (long i = 0; i < op.size(); ) {
                long currentCode = op.getOrDefault(i, 0L);
                switch ((int) (currentCode % 100)) {
                    case 1 -> {
                        Instruction inst = getInstruction(currentCode, op, i, 3);
                        op.put(inst.target, inst.val1 + inst.val2);
                        i += 4;
                    }
                    case 2 -> {
                        Instruction inst = getInstruction(currentCode, op, i, 3);
                        op.put(inst.target, inst.val1 * inst.val2);
                        i += 4;
                    }
                    case 3 -> {
                        Instruction inst = getInstruction(currentCode, op, i, 1);
                        op.put(inst.val1, in.take());
                        i += 2;
                    }
                    case 4 -> {
                        Instruction inst = getInstruction(currentCode, op, i, 1);
                        out.put(inst.val1);
                        i += 2;
                    }
                    case 5 -> {
                        Instruction inst = getInstruction(currentCode, op, i, 2);
                        if (inst.val1 != 0L) {
                            i = inst.val2;
                        } else {
                            i += 3;
                        }
                    }
                    case 6 -> {
                        Instruction inst = getInstruction(currentCode, op, i, 2);
                        if (inst.val1 == 0L) {
                            i = inst.val2;
                        } else {
                            i += 3;
                        }
                    }
                    case 7 -> {
                        Instruction inst = getInstruction(currentCode, op, i, 3);
                        op.put(inst.target, inst.val1 < inst.val2 ? 1L : 0L);
                        i += 4;
                    }
                    case 8 -> {
                        Instruction inst = getInstruction(currentCode, op, i, 3);
                        op.put(inst.target, inst.val1 == inst.val2 ? 1L : 0L);
                        i += 4;
                    }
                    case 9 -> {
                        relativeBase += getInstruction(currentCode, op, i, 1).val1;
                        i += 2;
                    }
                    case 99 -> {
                        return op.getOrDefault(0L, 0L);
                    }
                    default -> {
                        System.err.println("False opcode");
                        return -1L;
                    }
                }
            }
            return -1L;
        }

        private static class Instruction {
            long val1;
            long val2;
            long target;

            Instruction(long val1, long val2, long target) {
                this.val1 = val1;
                this.val2 = val2;
                this.target = target;
            }
        }
    }
}
