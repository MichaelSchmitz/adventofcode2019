package de.michaelschmitz.days;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Supplier;
import java.util.stream.Stream;


public class Day07 implements Day {
    private static final AmpThread[] queue = new AmpThread[5];
    private static List<Integer> results;

    private static void runCode(int i, int i2, int i3, int i4, int i5, String input) {
        try {
            List<LinkedBlockingQueue<String>> streams = Stream.generate((Supplier<LinkedBlockingQueue<String>>) LinkedBlockingQueue::new)
                    .limit(5).toList();
            streams.get(0).put(String.valueOf(i));
            streams.get(1).put(String.valueOf(i2));
            streams.get(2).put(String.valueOf(i3));
            streams.get(3).put(String.valueOf(i4));
            streams.get(4).put(String.valueOf(i5));
            streams.get(0).put("0");
            queue[0] = new AmpThread(streams.get(0), streams.get(1), input);
            queue[1] = new AmpThread(streams.get(1), streams.get(2), input);
            queue[2] = new AmpThread(streams.get(2), streams.get(3), input);
            queue[3] = new AmpThread(streams.get(3), streams.get(4), input);
            queue[4] = new AmpThread(streams.get(4), streams.get(0), input);
            queue[0].start();
            queue[1].start();
            queue[2].start();
            queue[3].start();
            queue[4].start();
            queue[0].join();
            queue[1].join();
            queue[2].join();
            queue[3].join();
            queue[4].join();
            results.add(Integer.parseInt(streams.get(0).take()));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public long executePartOne(List<String> input) {
        results = new LinkedList<>();
        for (int i = 0; i < 5; i++) {
            for (int i2 = 0; i2 < 5; i2++) {
                if (i2 != i) {
                    for (int i3 = 0; i3 < 5; i3++) {
                        if (i3 != i && i3 != i2) {
                            for (int i4 = 0; i4 < 5; i4++) {
                                if (i4 != i && i4 != i2 && i4 != i3) {
                                    for (int i5 = 0; i5 < 5; i5++) {
                                        if (i5 != i && i5 != i2 && i5 != i3 && i5 != i4) {
                                            runCode(i, i2, i3, i4, i5, input.get(0));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return results.stream().max(Comparator.comparingInt(Integer::valueOf)).orElse(-1);
    }

    @Override
    public long executePartTwo(List<String> input) {
        results = new LinkedList<>();
        for (int i = 5; i < 10; i++) {
            for (int i2 = 5; i2 < 10; i2++) {
                if (i2 != i) {
                    for (int i3 = 5; i3 < 10; i3++) {
                        if (i3 != i && i3 != i2) {
                            for (int i4 = 5; i4 < 10; i4++) {
                                if (i4 != i && i4 != i2 && i4 != i3) {
                                    for (int i5 = 5; i5 < 10; i5++) {
                                        if (i5 != i && i5 != i2 && i5 != i3 && i5 != i4) {
                                            runCode(i, i2, i3, i4, i5, input.get(0));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return results.stream().max(Comparator.comparingInt(Integer::valueOf)).orElse(-1);
    }


    private static class AmpThread extends Thread {
        BlockingQueue<String> in;
        BlockingQueue<String> out;
        String input;

        AmpThread(BlockingQueue<String> in, BlockingQueue<String> out, String input) {
            this.in = in;
            this.out = out;
            this.input = input;
        }

        public void run() {
            try {
                doProcessing(input);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        private Instruction getInstruction(String[] op, int i, boolean targetNeeded) {
            int val1;
            int val2;
            int target;
            if (op[i].length() > 2 && '1' == op[i].charAt(op[i].length() - 3)) {
                val1 = Integer.parseInt(op[i + 1]);
            } else {
                val1 = Integer.parseInt(op[Integer.parseInt(op[i + 1])]);
            }
            if (op[i].length() > 3 && '1' == op[i].charAt(op[i].length() - 4)) {
                val2 = Integer.parseInt(op[i + 2]);
            } else {
                val2 = Integer.parseInt(op[Integer.parseInt(op[i + 2])]);
            }
            if (targetNeeded)
                target = Integer.parseInt(op[i + 3]);
            else
                target = -1;
            return new Instruction(val1, val2, target);
        }

        void doProcessing(String input) throws InterruptedException {
            String[] op = input.split(",");
            for (int i = 0; i < op.length; ) {
                switch (op[i].substring(op[i].length() - 1)) {
                    case "1": {
                        Instruction inst = getInstruction(op, i, true);
                        op[inst.target] = String.valueOf(inst.val1 + inst.val2);
                        i += 4;
                        break;
                    }
                    case "2": {
                        Instruction inst = getInstruction(op, i, true);
                        op[inst.target] = String.valueOf(inst.val1 * inst.val2);
                        i += 4;
                        break;
                    }
                    case "3": {
                        op[Integer.parseInt(op[i + 1])] = in.take();
                        i += 2;
                        break;
                    }
                    case "4": {
                        int index;
                        if (op[i].length() > 2 && '1' == op[i].charAt(op[i].length() - 3)) {
                            index = Integer.parseInt(op[i + 1]);
                        } else {
                            index = Integer.parseInt(op[Integer.parseInt(op[i + 1])]);
                        }
                        out.put(String.valueOf(index));
                        i += 2;
                        break;
                    }
                    case "5": {
                        Instruction inst = getInstruction(op, i, false);
                        if (inst.val1 != 0) {
                            i = inst.val2;
                        } else {
                            i += 3;
                        }
                        break;
                    }
                    case "6": {
                        Instruction inst = getInstruction(op, i, false);
                        if (inst.val1 == 0) {
                            i = inst.val2;
                        } else {
                            i += 3;
                        }
                        break;
                    }
                    case "7": {
                        Instruction inst = getInstruction(op, i, true);
                        op[inst.target] = inst.val1 < inst.val2 ? "1" : "0";
                        i += 4;
                        break;
                    }
                    case "8": {
                        Instruction inst = getInstruction(op, i, true);
                        op[inst.target] = inst.val1 == inst.val2 ? "1" : "0";
                        i += 4;
                        break;
                    }
                    case "9": {
                        if (op[i].equals("99")) {
                            return;
                        }
                    }
                    default: {
                        System.err.println("False opcode");
                        return;
                    }
                }
            }
        }

        private static class Instruction {
            int val1;
            int val2;
            int target;

            Instruction(int val1, int val2, int target) {
                this.val1 = val1;
                this.val2 = val2;
                this.target = target;
            }
        }
    }
}
