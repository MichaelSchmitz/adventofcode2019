package de.michaelschmitz.days;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Stream;

public class Day14 implements Day {


    private static long produceFuel(Map<Element, List<Element>> reactions, long fuel) {
        Map<String, Long> needed = new HashMap<>(produceElement(reactions, "FUEL", fuel));
        while (needsProcessing(needed)) {
            Map.Entry<String, Long> entry = needed.entrySet().stream().filter(e -> !e.getKey().equals("ORE")).max(Comparator.comparingLong(e -> e.getKey().length() + e.getValue())).get();
            Map<String, Long> neededPart = produceElement(reactions, entry);
            needed.remove(entry.getKey());
            for (Map.Entry<String, Long> e : neededPart.entrySet()) {
                needed.put(e.getKey(), needed.getOrDefault(e.getKey(), 0L) + e.getValue());
            }
        }
        return needed.getOrDefault("ORE", -1L);
    }

    private static boolean needsProcessing(Map<String, Long> map) {
        if (map.size() == 1 && map.getOrDefault("ORE", -1L) != -1)
            return false;
        else {
            for (Map.Entry<String, Long> entry : map.entrySet()) {
                if (!entry.getKey().equals("ORE") && entry.getValue() > 0) {
                    return true;
                }
            }
        }
        return false;
    }


    private static Map<String, Long> produceElement(Map<Element, List<Element>> reactions, Map.Entry<String, Long> entry) {
        return produceElement(reactions, entry.getKey(), entry.getValue());
    }

    private static Map<String, Long> produceElement(Map<Element, List<Element>> reactions, String element, long count) {
        List<Element> parts = reactions.get(new Element(element, count));
        AtomicLong factor = new AtomicLong(-1);
        AtomicLong elCount = new AtomicLong(-1);
        reactions.keySet().forEach(el -> {
            if (el.name.equals(element)) {
                factor.set((long) Math.ceil((double) count / el.count));
                elCount.set(el.count);
            }
        });
        if (factor.get() == -1) {
            System.err.println("Factor not computed");
            return new HashMap<>();
        } else {
            Map<String, Long> result = new HashMap<>();
            for (Element e : parts) {
                result.put(e.name, e.count * factor.get());
            }
            long diff = elCount.get() * factor.get() - count;
            if (diff != 0) {
                result.put(element, -diff);
            }
            return result;
        }
    }


    private Map<Element, List<Element>> initMap(List<String> input) {
        Map<Element, List<Element>> reactions = new HashMap<>();
        input.forEach(inputLine -> {
            List<Element> list = extractComponents(inputLine.substring(0, inputLine.indexOf("=")));
            Element target = extractElement(inputLine.substring(inputLine.indexOf("=") + 2));
            reactions.put(target, list);
        });
        return reactions;
    }

    private static List<Element> extractComponents(String input) {
        List<Element> list = new LinkedList<>();
        String[] elements = input.split(",");
        for (String element : elements) {
            list.add(extractElement(element));
        }
        return list;
    }

    private static Element extractElement(String input) {
        String[] el = input.trim().split(" ");
        return new Element(el[1], Integer.parseInt(el[0]));

    }

    @Override
    public long executePartOne(List<String> input) {
        var reactions = initMap(input);
        return produceFuel(reactions, 1);
    }

    @Override
    public long executePartTwo(List<String> input) {
        var reactions = initMap(input);
        long fuel = 1;
        long one = produceFuel(reactions, fuel);
        long ore = one;
        while (ore < 1000000000000L) {
            fuel += (1000000000000L - ore) / one;
            ore = produceFuel(reactions, ++fuel);
        }
        while (ore > 1000000000000L) {
            ore = produceFuel(reactions, --fuel);
        }
        return fuel;
    }

    private static class Element {
        String name;
        long count;

        Element(String name, long count) {
            this.name = name;
            this.count = count;
        }

        @Override
        public int hashCode() {
            return name.hashCode();
        }

        @Override
        public boolean equals(Object o) {
            if (o instanceof Element) {
                return this.name.equals(((Element) o).name);
            }
            return false;
        }
    }
}
