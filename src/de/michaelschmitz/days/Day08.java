package de.michaelschmitz.days;

import java.util.LinkedList;
import java.util.List;

public class Day08 implements Day {
    private static final int boundary1 = 25;
    private static final int boundary2 = 6;

    @Override
    public long executePartOne(List<String> input) {
        long minZero = 151;
        long result = -1;
        var inputLine = input.get(0);
        for (int i = 0; i < inputLine.length(); i += 25 * 6) {
            String layer = inputLine.substring(i, i + 25 * 6);
            long count = layer.chars().filter(ch -> ch == '0').count();
            if (count < minZero) {
                minZero = count;
                result = layer.chars().filter(ch -> ch == '1').count() * layer.chars().filter(ch -> ch == '2').count();
            }
        }
        return result;
    }

    @Override
    public long executePartTwo(List<String> input) {
        List<String> layers = new LinkedList<>();
        var inputLine = input.get(0);
        for (int i = 0; i < inputLine.length(); i += boundary1 * boundary2) {
            layers.add(inputLine.substring(i, i + boundary1 * boundary2));
        }
        char[][] result = new char[boundary2][boundary1];
        for (String layer : layers) {
            for (int i = 0; i < boundary2; i++) {
                for (int k = 0; k < boundary1; k++) {
                    if (result[i][k] != '#' && result[i][k] != ' ') {
                        char c = layer.charAt(i * boundary1 + k);
                        result[i][k] = c > '1' ? c : c == '0' ? ' ' : '#';
                    }
                }
            }
        }
        for (char[] res : result) {
            System.out.println(res);
        }
        return -1;
    }
}
