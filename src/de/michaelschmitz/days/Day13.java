package de.michaelschmitz.days;

import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Day13 implements Day {
    private static final boolean PRINT_GAME = false;

    private static void printGame(Map<Position, Tile> game) {
        if (PRINT_GAME) {
            System.out.print("\033[H\033[2J");
            System.out.flush();
            var posDef = new Position(0, 0);
            int maxX = game.keySet().stream().max(Comparator.comparingInt(pos -> pos.x)).orElse(posDef).x;
            int maxY = game.keySet().stream().max(Comparator.comparingInt(pos -> pos.y)).orElse(posDef).y;
            int minX = game.keySet().stream().min(Comparator.comparingInt(pos -> pos.x)).orElse(posDef).x;
            int minY = game.keySet().stream().min(Comparator.comparingInt(pos -> pos.y)).orElse(posDef).y;
            for (int y = minY; y <= maxY; y++) {
                for (int x = minX; x <= maxX; x++) {
                    System.out.print(game.getOrDefault(new Position(x, y), new Tile(0)).getTile());

                }
                System.out.println();
            }
        }
    }

    private long runCode(long start, Map<Position, Tile> game, String input) {
        try {
            LinkedBlockingQueue<Long> queue = new LinkedBlockingQueue<>(3);
            IntcodeThread intcodeThread = new IntcodeThread(queue, start, game, input);
            GameThread gameThread = new GameThread(queue, game);
            GameThread.runGame = true;
            gameThread.start();
            intcodeThread.start();
            intcodeThread.join();
            GameThread.runGame = false;
            gameThread.join();
            return gameThread.result;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public long executePartOne(List<String> input) {
        Map<Position, Tile> game = new HashMap<>();
        runCode(1L, game, input.get(0));
        printGame(game);
        return game.values().stream().filter(tile -> tile.getTile() == '□').count();
    }

    @Override
    public long executePartTwo(List<String> input) {
        Map<Position, Tile> game = new HashMap<>();
        return runCode(2L, game, input.get(0));
    }

    private static class Position {
        int x;
        int y;

        Position(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public Position(long x, long y) {
            this.x = (int) x;
            this.y = (int) y;
        }

        @Override
        public boolean equals(Object o) {
            if (o instanceof Position) {
                return this.x == ((Position) o).x && this.y == ((Position) o).y;
            }
            return false;
        }

        @Override
        public int hashCode() {
            return (x + "," + y).hashCode();
        }
    }

    private static class Tile {
        int tile;

        Tile(int tile) {
            this.tile = tile;
        }

        public char getTile() {
            return switch (tile) {
                case 0 -> ' ';
                case 1 -> '█';
                case 2 -> '□';
                case 3 -> '▔';
                case 4 -> '●';
                default -> 'x';
            };
        }
    }

    private static class GameThread extends Thread {
        final BlockingQueue<Long> in;
        final Map<Position, Tile> game;
        static boolean runGame = true;
        long result = -1;

        GameThread(BlockingQueue<Long> in, Map<Position, Tile> game) {
            this.in = in;
            this.game = game;
        }

        @Override
        public void run() {
            try {
                while (runGame) {
                    synchronized (in) {
                        if (in.size() >= 3) {
                            long x = in.take();
                            long y = in.take();
                            long tile = in.take();
                            if (x == -1 && y == 0) {
                                result = tile;
                            } else {
                                synchronized (game) {
                                    game.put(new Position(x, y), new Tile(Math.toIntExact(tile)));
                                }
                            }
                            in.notify();
                        }
                    }
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }


    private static class IntcodeThread extends Thread {
        long relativeBase = 0;
        final BlockingQueue<Long> out;
        long start;
        final Map<Position, Tile> game;
        String input;
        long result = -1;

        IntcodeThread(BlockingQueue<Long> out, long start, Map<Position, Tile> game, String input) {
            this.out = out;
            this.start = start;
            this.game = game;
            this.input = input;
        }

        private long getMovement() {
            synchronized (game) {
                int ballX = 0;
                int padX = 0;
                try {
                    ballX = game.entrySet().stream().filter(tile -> tile.getValue().getTile() == '●').findFirst().get().getKey().x;
                } catch (NoSuchElementException ignored) {
                }
                try {
                    padX = game.entrySet().stream().filter(tile -> tile.getValue().getTile() == '▔').findFirst().get().getKey().x;
                } catch (NoSuchElementException ignored) {
                }
                if (padX > ballX)
                    return -1;
                else if (padX < ballX)
                    return 1;
                return 0;
            }
        }

        public void run() {
            try {
                result = doProcessing();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        private Instruction getInstruction(long inst, Map<Long, Long> op, long ip, int size) {
            long val1 = getParam(op, ip, getMode(inst, 1), 1, inst % 10 == 3);
            long val2 = -1;
            long val3 = -1;
            if (size >= 2)
                val2 = getParam(op, ip, getMode(inst, 2), 2, inst % 10 == 3);
            if (size >= 3)
                val3 = getParam(op, ip, getMode(inst, 3), 3, true);
            return new Instruction(val1, val2, val3);
        }

        private long getParam(Map<Long, Long> op, long ip, int mode, int pos, boolean rel) {
            Long tmp = op.getOrDefault(ip + pos, 0L);
            switch (mode) {
                case 0:
                    return rel ? tmp : op.getOrDefault(tmp, 0L);
                case 1:
                    return tmp;
                case 2:
                    return rel ? relativeBase + tmp : op.getOrDefault(relativeBase + tmp, 0L);
                default:
                    System.err.println("Wrong mode");
                    return -1;
            }
        }

        private int getMode(long inst, int pos) {
            return (int) (inst / Math.pow(10, pos + 1) % 10);
        }

        long doProcessing() throws InterruptedException {
            String[] opTmp = input.split(",");
            Map<Long, Long> op = new HashMap<>();
            for (long i = 0; i < opTmp.length; i++) {
                op.put(i, Long.parseLong(opTmp[(int) i]));
            }
            op.put(0L, start);
            for (long i = 0; i < op.size(); ) {
                long currentCode = op.getOrDefault(i, 0L);
                switch ((int) (currentCode % 100)) {
                    case 1 -> {
                        Instruction inst = getInstruction(currentCode, op, i, 3);
                        op.put(inst.target, inst.val1 + inst.val2);
                        i += 4;
                    }
                    case 2 -> {
                        Instruction inst = getInstruction(currentCode, op, i, 3);
                        op.put(inst.target, inst.val1 * inst.val2);
                        i += 4;
                    }
                    case 3 -> {
                        Instruction inst = getInstruction(currentCode, op, i, 1);
                        printGame(game);
                        op.put(inst.val1, getMovement());
                        i += 2;
                    }
                    case 4 -> {
                        Instruction inst = getInstruction(currentCode, op, i, 1);
                        synchronized (out) {
                            out.put(inst.val1);
                            if (out.remainingCapacity() == 0) {
                                out.wait();
                            }
                        }
                        i += 2;
                    }
                    case 5 -> {
                        Instruction inst = getInstruction(currentCode, op, i, 2);
                        if (inst.val1 != 0L) {
                            i = inst.val2;
                        } else {
                            i += 3;
                        }
                    }
                    case 6 -> {
                        Instruction inst = getInstruction(currentCode, op, i, 2);
                        if (inst.val1 == 0L) {
                            i = inst.val2;
                        } else {
                            i += 3;
                        }
                    }
                    case 7 -> {
                        Instruction inst = getInstruction(currentCode, op, i, 3);
                        op.put(inst.target, inst.val1 < inst.val2 ? 1L : 0L);
                        i += 4;
                    }
                    case 8 -> {
                        Instruction inst = getInstruction(currentCode, op, i, 3);
                        op.put(inst.target, inst.val1 == inst.val2 ? 1L : 0L);
                        i += 4;
                    }
                    case 9 -> {
                        relativeBase += getInstruction(currentCode, op, i, 1).val1;
                        i += 2;
                    }
                    case 99 -> {
                        return op.getOrDefault(0L, 0L);
                    }
                    default -> {
                        System.err.println("False opcode");
                        return -1L;
                    }
                }
            }
            return -1L;
        }

        private static class Instruction {
            long val1;
            long val2;
            long target;

            Instruction(long val1, long val2, long target) {
                this.val1 = val1;
                this.val2 = val2;
                this.target = target;
            }
        }
    }
}
