package de.michaelschmitz.days;

import java.util.List;

public class Day12 implements Day {
    private Moon io;
    private Moon europa;
    private Moon ganymede;
    private Moon callisto;

    private Moon ioStart;
    private Moon europaStart;
    private Moon ganymedeStart;
    private Moon callistoStart;

    static void execute2() {
    }

    private static long lcm(long a, long b) {
        long lcm = 0;
        while ((lcm += Math.max(a, b)) % Math.min(a, b) != 0) ;
        return lcm;

    }

    private boolean getMoonPosX() {
        return io.pos.x == ioStart.pos.x && europa.pos.x == europaStart.pos.x && ganymede.pos.x == ganymedeStart.pos.x && callisto.pos.x == callistoStart.pos.x && io.vel.x == 0 && europa.vel.x == 0 && ganymede.vel.x == 0 && callisto.vel.x == 0;
    }

    private boolean getMoonPosY() {
        return io.pos.y == ioStart.pos.y && europa.pos.y == europaStart.pos.y && ganymede.pos.y == ganymedeStart.pos.y && callisto.pos.y == callistoStart.pos.y && io.vel.y == 0 && europa.vel.y == 0 && ganymede.vel.y == 0 && callisto.vel.y == 0;
    }

    private boolean getMoonPosZ() {
        return io.pos.z == ioStart.pos.z && europa.pos.z == europaStart.pos.z && ganymede.pos.z == ganymedeStart.pos.z && callisto.pos.z == callistoStart.pos.z && io.vel.z == 0 && europa.vel.z == 0 && ganymede.vel.z == 0 && callisto.vel.z == 0;
    }

    private void loadStart(List<String> input) {
        io = loadMoon(input.get(0));
        europa = loadMoon(input.get(1));
        ganymede = loadMoon(input.get(2));
        callisto = loadMoon(input.get(3));
        ioStart = loadMoon(input.get(0));
        europaStart = loadMoon(input.get(1));
        ganymedeStart = loadMoon(input.get(2));
        callistoStart = loadMoon(input.get(3));
    }

    private Moon loadMoon(String in) {
        String[] coord = in.substring(1, in.indexOf(">")).replaceAll(".=", "").split(", ");
        return new Moon(Integer.parseInt(coord[0]), Integer.parseInt(coord[1]), Integer.parseInt(coord[2]));
    }

    private int getEnergyTotal() {
        return io.toEnergy() + europa.toEnergy() + ganymede.toEnergy() + callisto.toEnergy();
    }

    private void applyGravity() {
        applyGravity(io, europa);
        applyGravity(io, ganymede);
        applyGravity(io, callisto);
        applyGravity(europa, ganymede);
        applyGravity(europa, callisto);
        applyGravity(ganymede, callisto);
    }

    private static void applyGravity(Moon m1, Moon m2) {
        if (m1.pos.x > m2.pos.x) {
            m1.vel.x--;
            m2.vel.x++;
        } else if (m1.pos.x < m2.pos.x) {
            m1.vel.x++;
            m2.vel.x--;
        }
        if (m1.pos.y > m2.pos.y) {
            m1.vel.y--;
            m2.vel.y++;
        } else if (m1.pos.y < m2.pos.y) {
            m1.vel.y++;
            m2.vel.y--;
        }
        if (m1.pos.z > m2.pos.z) {
            m1.vel.z--;
            m2.vel.z++;
        } else if (m1.pos.z < m2.pos.z) {
            m1.vel.z++;
            m2.vel.z--;
        }
    }

    private void moveMoons() {
        moveMoon(io);
        moveMoon(europa);
        moveMoon(ganymede);
        moveMoon(callisto);
    }

    private void moveMoon(Moon moon) {
        moon.pos.x += moon.vel.x;
        moon.pos.y += moon.vel.y;
        moon.pos.z += moon.vel.z;
    }

    @Override
    public long executePartOne(List<String> input) {
        loadStart(input);
        for (int i = 0; i < 1000; i++) {
            applyGravity();
            moveMoons();
        }
        return getEnergyTotal();
    }

    @Override
    public long executePartTwo(List<String> input) {
        loadStart(input);
        long i = 0;
        long x = -1;
        long y = -1;
        long z = -1;
        while (true) {
            applyGravity();
            moveMoons();
            i++;
            if (x == -1 && getMoonPosX())
                x = i;
            if (y == -1 && getMoonPosY())
                y = i;
            if (z == -1 && getMoonPosZ())
                z = i;
            if (x != -1 && y != -1 && z != -1) {
                return lcm(lcm(x, y), z);
            }
        }
    }

    private static class Moon {
        D3 pos;
        D3 vel;

        Moon(int x, int y, int z) {
            pos = new D3(x, y, z);
            vel = new D3();
        }

        private int toPotEnergy() {
            return Math.abs(pos.x) + Math.abs(pos.y) + Math.abs(pos.z);
        }

        private int toKinEnergy() {
            return Math.abs(vel.x) + Math.abs(vel.y) + Math.abs(vel.z);
        }

        int toEnergy() {
            return toKinEnergy() * toPotEnergy();
        }

        private static class D3 {
            int x;
            int y;
            int z;

            D3() {
                x = 0;
                y = 0;
                z = 0;
            }

            D3(int x, int y, int z) {
                this.x = x;
                this.y = y;
                this.z = z;
            }
        }
    }
}
