package de.michaelschmitz.days;

import java.util.List;

public class Day05 implements Day {
    private static Instruction getInstruction(String[] op, int i, boolean targetNeeded) {
        int val1;
        int val2;
        int target;
        if (op[i].length() > 2 && '1' == op[i].charAt(op[i].length() - 3)) {
            val1 = Integer.parseInt(op[i + 1]);
        } else {
            val1 = Integer.parseInt(op[Integer.parseInt(op[i + 1])]);
        }
        if (op[i].length() > 3 && '1' == op[i].charAt(op[i].length() - 4)) {
            val2 = Integer.parseInt(op[i + 2]);
        } else {
            val2 = Integer.parseInt(op[Integer.parseInt(op[i + 2])]);
        }
        if (targetNeeded)
            target = Integer.parseInt(op[i + 3]);
        else
            target = -1;
        return new Instruction(val1, val2, target);
    }

    private int runComputer(String[] op, String startVal, boolean useExtendedOpCodes) {
        for (int i = 0; i < op.length; ) {
            switch (op[i].substring(op[i].length() - 1)) {
                case "1": {
                    var inst = getInstruction(op, i, true);
                    op[inst.target] = String.valueOf(inst.val1 + inst.val2);
                    i += 4;
                    break;
                }
                case "2": {
                    var inst = getInstruction(op, i, true);
                    op[inst.target] = String.valueOf(inst.val1 * inst.val2);
                    i += 4;
                    break;
                }
                case "3": {
                    op[Integer.parseInt(op[i + 1])] = startVal;
                    i += 2;
                    break;
                }
                case "4": {
                    int index;
                    if (op[i].length() > 2 && '1' == op[i].charAt(op[i].length() - 3)) {
                        index = Integer.parseInt(op[i + 1]);
                    } else {
                        index = Integer.parseInt(op[Integer.parseInt(op[i + 1])]);
                    }
                    i += 2;
                    if (index == 0)
                        break;
                    else
                        return index;
                }
                case "5": {
                    if (useExtendedOpCodes) {
                        var inst = getInstruction(op, i, false);
                        if (inst.val1 != 0) {
                            i = inst.val2;
                        } else {
                            i += 3;
                        }
                    } else {
                        i += 3;
                    }
                    break;
                }
                case "6": {
                    if (useExtendedOpCodes) {
                        var inst = getInstruction(op, i, false);
                        if (inst.val1 == 0) {
                            i = inst.val2;
                        } else {
                            i += 3;
                        }
                    } else {
                        i += 3;
                    }
                    break;
                }
                case "7": {
                    if (useExtendedOpCodes) {
                        var inst = getInstruction(op, i, true);
                        op[inst.target] = inst.val1 < inst.val2 ? "1" : "0";
                    }
                    i += 4;
                    break;
                }
                case "8": {
                    if (useExtendedOpCodes) {
                        var inst = getInstruction(op, i, true);
                        op[inst.target] = inst.val1 == inst.val2 ? "1" : "0";
                    }
                    i += 4;
                    break;
                }
                case "9": {
                    if (op[i].equals("99")) {
                        return Integer.parseInt(op[0]);
                    }
                }
                default: {
                    return -1;
                }
            }
        }
        return -1;
    }

    @Override
    public long executePartOne(List<String> input) {
        String[] op = input.get(0).split(",");
        return runComputer(op, "1", false);
    }

    @Override
    public long executePartTwo(List<String> input) {
        String[] op = input.get(0).split(",");
        return runComputer(op, "5", true);
    }

    private static class Instruction {
        int val1;
        int val2;
        int target;

        Instruction(int val1, int val2, int target) {
            this.val1 = val1;
            this.val2 = val2;
            this.target = target;
        }
    }

}
